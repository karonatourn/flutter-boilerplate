import 'package:flutter_boilerplate/features/login/data/usecases/post_login.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter_boilerplate/features/login/presentation/bloc/login_bloc.dart';
import 'package:flutter_boilerplate/features/login/data/repositories/login_repo.dart';
import 'package:flutter_boilerplate/features/login/data/repositories/login_repo_impl.dart';

Future<void> locateLoginFeatureService (GetIt sl) async {

  sl.registerLazySingleton<LoginRepo>(() => LoginRepoImpl());
  sl.registerLazySingleton<PostLogin>(() => PostLogin(sl<LoginRepo>()));
  sl.registerFactory<LoginBloc>(() => LoginBloc(postLogin: sl<PostLogin>()));

}