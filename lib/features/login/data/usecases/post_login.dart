import 'package:flutter_boilerplate/core/models/auth_model.dart';
import 'package:flutter_boilerplate/core/usecases/usecase.dart';
import 'package:flutter_boilerplate/features/login/data/repositories/login_repo.dart';

class PostLogin extends UseCase<AuthModel, LoginParams> {

  final LoginRepo repo;

  PostLogin(this.repo);

  @override
  Future<AuthModel> call([LoginParams? params]) async {

    return repo.postLogin(phoneNumber: params?.phoneNumber ?? "", password: params?.password ?? "");

  }

}

class LoginParams extends Params {

  final String phoneNumber;
  final String password;

  const LoginParams({required this.phoneNumber, required this.password});

}