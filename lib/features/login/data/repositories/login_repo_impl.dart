import 'package:flutter_boilerplate/core/config/app_config.dart';
import 'package:flutter_boilerplate/core/models/auth_model.dart';
import 'package:flutter_boilerplate/core/models/auth_request_model.dart';
import 'package:flutter_boilerplate/core/services/index.dart';
import 'package:flutter_boilerplate/features/login/data/repositories/login_repo.dart';

class LoginRepoImpl extends LoginRepo {

  @override
  Future<AuthModel> postLogin({
    required String phoneNumber,
    required String password,
  }) async {

    var response = await HttpService().httpClient.post(
      '/client/auth/login',
      data: AuthRequestModel(
        phoneNumber: phoneNumber,
        password: password,
        countryCode: AppConfig.countryCode,
      ).toJson()
    );

    var data = AuthModel.fromJson(response.data["data"]);

    return data;

  }

}
