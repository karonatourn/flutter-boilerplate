import 'package:flutter_boilerplate/core/models/auth_model.dart';

abstract class LoginRepo {

  Future<AuthModel> postLogin({
    required String phoneNumber,
    required String password,
  });

}
