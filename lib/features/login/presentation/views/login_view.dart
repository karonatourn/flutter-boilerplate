import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/core/styles/index.dart';
import 'package:flutter_boilerplate/core/widgets/layout/bloc_state_view.dart';
import 'package:flutter_boilerplate/features/login/presentation/bloc/login_bloc.dart';
import 'package:flutter_boilerplate/features/login/presentation/bloc/login_event.dart';
import 'package:flutter_boilerplate/generated/locale_keys.g.dart';
import 'package:flutter_boilerplate/injection_container.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    phoneNumberController.text = "010522989";
    passwordController.text = "1234";
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<LoginBloc>(),
      child: Scaffold(
        body: BlocStateView<LoginBloc>(
          builder: (context, state) {
            return Form(
              key: _formKey,
              child: Container(
                padding: const EdgeInsets.all(16),
                decoration: const BoxDecoration(
                  color: AppColors.screenBackground,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: CachedNetworkImage(
                        imageUrl: 'https://erp-staging.codingate.com/web/image/website/1/logo/My%20Website?unique=d27f5a1',
                        width: 200,
                        height: 90,
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 2,
                      child: Column(
                        children: [
                          TextFormField(
                            controller: phoneNumberController,
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              labelText: LocaleKeys.login_phoneNumber.tr(),
                              floatingLabelStyle: const TextStyle(
                                backgroundColor: Colors.white,
                              ),
                              hintText: LocaleKeys.login_phoneNumber.tr(),
                              border: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black, width: 1.5),
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                              ),
                              prefixIcon: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8),
                                child: Image.asset(
                                  "lib/features/login/assets/images/icons/ic_phone_number.png",
                                  width: 10,
                                  height: 10,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 16,),
                          TextFormField(
                            controller: passwordController,
                            keyboardType: TextInputType.visiblePassword,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              labelText: LocaleKeys.login_password.tr(),
                              floatingLabelStyle: const TextStyle(
                                backgroundColor: Colors.white,
                              ),
                              hintText: LocaleKeys.login_password.tr(),
                              border: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black, width: 1.5),
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                              ),
                              prefixIcon: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8),
                                child: Image.asset(
                                  "lib/features/login/assets/images/icons/ic_password.png",
                                  width: 10,
                                  height: 10,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 16,),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(primary: AppColors.primary, side: const BorderSide(color: Colors.black, width: 1.5)),
                            onPressed: () {
                              context.read<LoginBloc>().add(LoginStarted(phoneNumberController.text, passwordController.text));
                            },
                            child: Text(LocaleKeys.login_login.tr().toUpperCase(), style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white),),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
