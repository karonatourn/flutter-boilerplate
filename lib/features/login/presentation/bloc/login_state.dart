import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';

class LoginInitial extends BlocStateInitial {}

class LoginInProgress extends BlocStateInProgress {}

class LoginSuccess extends BlocStateSuccess {}

class LoginFailure extends BlocStateFailure {

  const LoginFailure() : super(message: "");

  LoginFailure.from(BlocStateFailure other): super.from(other);

}