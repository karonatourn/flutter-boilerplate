import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_event.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';
import 'package:flutter_boilerplate/core/models/auth_model.dart';
import 'package:flutter_boilerplate/core/repositories/local_repo.dart';
import 'package:flutter_boilerplate/core/services/navigation_service.dart';
import 'package:flutter_boilerplate/core/utils/helper_functions.dart';
import 'package:flutter_boilerplate/features/home/presentation/views/home_view.dart';
import 'package:flutter_boilerplate/features/login/data/usecases/post_login.dart';
import 'package:flutter_boilerplate/features/login/presentation/bloc/login_event.dart';
import 'package:flutter_boilerplate/features/login/presentation/bloc/login_state.dart';

class LoginBloc extends Bloc<BlocEvent, BlocState> {

  final PostLogin postLogin;

  LoginBloc({required this.postLogin}) : super(LoginInitial()) {

    on<ErrorDialogDismissEvent>((event, emit) {
      emit(LoginInitial());
    });

    on<LoginStarted>((event, emit) async {
      try {
        emit(LoginInProgress());

        var response = await postLogin(LoginParams(phoneNumber: event.phoneNumber, password: event.password));
        await LocalRepo().saveAuth(AuthModel.fromJson(response.toJson()));

        emit(LoginSuccess());

        NavigationService().replace(const HomeView());
      } catch (error) {
        var failureState = HelperFunctions.handleErrorToState(error);
        emit(LoginFailure.from(failureState));
      }
    });

  }

}
