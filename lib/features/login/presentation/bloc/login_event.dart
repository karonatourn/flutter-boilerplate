import 'package:flutter_boilerplate/core/bloc/bloc_event.dart';

class LoginStarted extends BlocEvent {
  final String phoneNumber;
  final String password;

  const LoginStarted(
    this.phoneNumber,
    this.password,
  );
}
