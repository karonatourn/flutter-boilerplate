import 'package:get_it/get_it.dart';
import 'package:flutter_boilerplate/features/home/data/usecases/get_profile.dart';

import 'data/repositories/home_reqo_impl.dart';
import 'data/repositories/home_repo.dart';
import 'presentation/blocs/home_bloc.dart';

Future<void> locateHomeFeatureService(GetIt sl) async {

  sl.registerLazySingleton<HomeRepo>(() => HomeRepoImpl());
  sl.registerLazySingleton<GetProfile>(() => GetProfile(sl<HomeRepo>()));
  sl.registerFactory<HomeBloc>(() => HomeBloc(getUser: sl<GetProfile>()));

}