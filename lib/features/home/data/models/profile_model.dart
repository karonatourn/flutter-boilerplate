class ProfileModel {
  ProfileModel({
      this.id, 
      this.gender, 
      this.phoneNumber, 
      this.abaAccount, 
      this.abaAccountName, 
      this.countryCode, 
      this.address, 
      this.email, 
      this.clientId, 
      this.zoneId, 
      this.zoneName, 
      this.type, 
      this.referralId, 
      this.code, 
      this.fullName, 
      this.birthday, 
      this.profile,});

  ProfileModel.fromJson(dynamic json) {
    id = json['id'];
    gender = json['gender'];
    phoneNumber = json['phone_number'];
    abaAccount = json['aba_account'];
    abaAccountName = json['aba_account_name'];
    countryCode = json['country_code'];
    address = json['address'];
    email = json['email'];
    clientId = json['client_id'];
    zoneId = json['zone_id'];
    zoneName = json['zone_name'];
    type = json['type'];
    referralId = json['referral_id'];
    code = json['code'];
    fullName = json['full_name'];
    birthday = json['birthday'];
    profile = json['profile'];
  }
  int? id;
  String? gender;
  String? phoneNumber;
  dynamic abaAccount;
  dynamic abaAccountName;
  dynamic countryCode;
  String? address;
  String? email;
  int? clientId;
  int? zoneId;
  String? zoneName;
  String? type;
  String? referralId;
  String? code;
  String? fullName;
  String? birthday;
  String? profile;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['gender'] = gender;
    map['phone_number'] = phoneNumber;
    map['aba_account'] = abaAccount;
    map['aba_account_name'] = abaAccountName;
    map['country_code'] = countryCode;
    map['address'] = address;
    map['email'] = email;
    map['client_id'] = clientId;
    map['zone_id'] = zoneId;
    map['zone_name'] = zoneName;
    map['type'] = type;
    map['referral_id'] = referralId;
    map['code'] = code;
    map['full_name'] = fullName;
    map['birthday'] = birthday;
    map['profile'] = profile;
    return map;
  }

}