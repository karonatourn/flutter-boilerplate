import 'package:flutter_boilerplate/core/usecases/usecase.dart';
import 'package:flutter_boilerplate/features/home/data/models/profile_model.dart';
import 'package:flutter_boilerplate/features/home/data/repositories/home_repo.dart';

class GetProfile extends UseCase<ProfileModel, NoParams> {

  final HomeRepo repo;

  GetProfile(this.repo);

  @override
  Future<ProfileModel> call([NoParams? params]) async {

    return repo.getProfile();

  }

}
