import 'package:flutter_boilerplate/core/services/index.dart';
import 'package:flutter_boilerplate/features/home/data/models/profile_model.dart';
import 'package:flutter_boilerplate/features/home/data/repositories/home_repo.dart';

class HomeRepoImpl extends HomeRepo {

  @override
  Future<ProfileModel> getProfile() async {

    var response = await HttpService().authGet('/client/profile');
    var data = ProfileModel.fromJson(response.data["data"]);

    return data;

  }

}
