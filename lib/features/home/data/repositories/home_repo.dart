import 'package:flutter_boilerplate/features/home/data/models/profile_model.dart';

abstract class HomeRepo {

  Future<ProfileModel> getProfile();

}
