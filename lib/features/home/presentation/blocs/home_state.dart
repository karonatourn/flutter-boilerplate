import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';
import 'package:flutter_boilerplate/features/home/data/models/profile_model.dart';

class HomeInProgress extends BlocStateInProgress {}

class HomeSuccess extends BlocStateSuccess {

  final ProfileModel? profile;

  HomeSuccess({required this.profile}) : super();

}

class HomeFailure extends BlocStateFailure {

  const HomeFailure() : super(message: "");

}
