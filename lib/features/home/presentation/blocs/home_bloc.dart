import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_event.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';
import 'package:flutter_boilerplate/core/utils/helper_functions.dart';
import 'package:flutter_boilerplate/features/home/data/usecases/get_profile.dart';
import 'package:flutter_boilerplate/features/home/presentation/blocs/home_event.dart';
import 'package:flutter_boilerplate/features/home/presentation/blocs/home_state.dart';

class HomeBloc extends Bloc<BlocEvent, BlocState> {

  final GetProfile getUser;

  HomeBloc({required this.getUser}) : super(HomeSuccess(profile: null)) {
    on<HomeStarted>(_onGetUser);
  }

  void _onGetUser(HomeStarted event, Emitter<BlocState> emit) async {
    try {
      emit(HomeInProgress());

      final user = await getUser();

      emit(HomeSuccess(profile: user));
    } catch (error) {
      HelperFunctions.handleErrorToState(error, navigateToLogin: true);
      emit(const HomeFailure());
    }
  }

}
