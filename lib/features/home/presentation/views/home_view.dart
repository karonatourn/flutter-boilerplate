import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/core/styles/index.dart';
import 'package:flutter_boilerplate/core/widgets/layout/bloc_state_view.dart';
import 'package:flutter_boilerplate/features/home/data/models/profile_model.dart';
import 'package:flutter_boilerplate/features/home/data/usecases/get_profile.dart';
import 'package:flutter_boilerplate/features/home/presentation/blocs/home_bloc.dart';
import 'package:flutter_boilerplate/features/home/presentation/blocs/home_event.dart';
import 'package:flutter_boilerplate/features/home/presentation/blocs/home_state.dart';
import 'package:flutter_boilerplate/injection_container.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => HomeBloc(getUser: sl<GetProfile>()),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
          backgroundColor: AppColors.primary,
        ),
        body: BlocStateView<HomeBloc>(
          onInitState: (context) {
            context.read<HomeBloc>().add(HomeStarted());
          },
          builder: (context, state) {
            ProfileModel? profile;

            if (state is HomeSuccess) {
              final _state = state;
              profile = _state.profile;
            }

            return Container(
              decoration: const BoxDecoration(color: AppColors.screenBackground,),
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 1.5,),
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    boxShadow: kElevationToShadow[4],
                  ),
                  padding: const EdgeInsets.all(16),
                  width: MediaQuery.of(context).size.width - 40,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CircleAvatar(
                        radius: 60,
                        backgroundImage: NetworkImage(profile?.profile ?? ""),
                      ),
                      const SizedBox(height: 16,),
                      Text(profile?.fullName ?? "N/A", style: Theme.of(context).textTheme.headline6,),
                      const SizedBox(height: 20,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text("Full name: ", style: Theme.of(context).textTheme.bodyText2,),
                              Text(profile?.fullName ?? "N/A", style: Theme.of(context).textTheme.bodyText1,),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text("Gender: ", style: Theme.of(context).textTheme.bodyText2,),
                              Text(profile?.gender ?? "N/A", style: Theme.of(context).textTheme.bodyText1,),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text("Birthday: ", style: Theme.of(context).textTheme.bodyText2,),
                              Text(profile?.birthday ?? "N/A", style: Theme.of(context).textTheme.bodyText1,),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text("Phone number: ", style: Theme.of(context).textTheme.bodyText2,),
                              Text(profile?.phoneNumber ?? "N/A", style: Theme.of(context).textTheme.bodyText1,),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text("Email: ", style: Theme.of(context).textTheme.bodyText2,),
                              Text(profile?.email ?? "N/A", style: Theme.of(context).textTheme.bodyText1,),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Address: ", style: Theme.of(context).textTheme.bodyText2,),
                              Flexible(child: Text(profile?.address ?? "N/A", style: Theme.of(context).textTheme.bodyText1, softWrap: true,),),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
