import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/core/styles/index.dart';
import 'package:flutter_boilerplate/core/widgets/layout/bloc_state_view.dart';
import 'package:flutter_boilerplate/features/splash/presentation/blocs/splash_bloc.dart';
import 'package:flutter_boilerplate/features/splash/presentation/blocs/splash_event.dart';
import 'package:flutter_boilerplate/injection_container.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<SplashBloc>(),
      child: Scaffold(
        body: BlocStateView<SplashBloc>(
          onInitState: (BuildContext context) {
            context.read<SplashBloc>().add(SplashStarted());
          },
          builder: (context, state) {
            return Container(
              decoration: const BoxDecoration(
                color: AppColors.primary,
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CachedNetworkImage(
                      imageUrl: 'https://erp-staging.codingate.com/web/image/website/1/logo/My%20Website?unique=d27f5a1',
                      width: 200,
                      height: 90,
                    ),
                    const SizedBox(
                      height: 80,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
