import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_event.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';
import 'package:flutter_boilerplate/core/services/index.dart';
import 'package:flutter_boilerplate/features/login/presentation/views/login_view.dart';
import 'package:flutter_boilerplate/features/splash/presentation/blocs/splash_event.dart';
import 'package:flutter_boilerplate/features/splash/presentation/blocs/splash_state.dart';

class SplashBloc extends Bloc<BlocEvent, BlocState> {
  SplashBloc() : super(SplashInProgress()) {
    on<SplashStarted>(_onStartLoading);
  }

  Future<void> _onStartLoading(SplashStarted event, Emitter<BlocState> emit) async {
    try {
      emit(SplashInProgress());

      // Init local storage service
      await LocalStorageService().init();
      // Init device info service
      await DeviceInfoService().init();
      // Init Http service
      await HttpService().init();
      // Init socket service
      // await SocketService().init();

      await Future.delayed(
        const Duration(seconds: 1),
        () {
          emit(SplashSuccess());

          NavigationService().replace(const LoginView());
        },
      );
    } catch (error) {
      emit(const SplashFailure());
    }
  }
}
