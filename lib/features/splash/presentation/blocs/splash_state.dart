import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';

class SplashInProgress extends BlocStateInProgress {}

class SplashSuccess extends BlocStateSuccess {}

class SplashFailure extends BlocStateFailure {

  const SplashFailure() : super(message: "");

}
