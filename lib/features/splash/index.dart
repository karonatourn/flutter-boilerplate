import 'package:get_it/get_it.dart';
import 'presentation/blocs/splash_bloc.dart';

Future<void> locateSplashFeatureService (GetIt sl) async {

  sl.registerFactory<SplashBloc>(() => SplashBloc());

}