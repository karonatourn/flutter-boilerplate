import 'package:get_it/get_it.dart';
import 'package:flutter_boilerplate/features/home/index.dart';
import 'package:flutter_boilerplate/features/login/index.dart';
import 'package:flutter_boilerplate/features/splash/index.dart';

/// sl is referred to as Service Locator
final sl = GetIt.instance;

/// Called in main to initialize service locator
Future<void> init() async {
  // Features
  await locateSplashFeatureService(sl);
  await locateLoginFeatureService(sl);
  await locateHomeFeatureService(sl);
}
