// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const common_pleaseWait = 'common.pleaseWait';
  static const common_processingYourRequest = 'common.processingYourRequest';
  static const common_somethingWrong = 'common.somethingWrong';
  static const common_buttonOk = 'common.buttonOk';
  static const common_buttonCancel = 'common.buttonCancel';
  static const common_buttonYes = 'common.buttonYes';
  static const common_buttonNo = 'common.buttonNo';
  static const common_buttonClose = 'common.buttonClose';
  static const common = 'common';
  static const login_phoneNumber = 'login.phoneNumber';
  static const login_password = 'login.password';
  static const login_login = 'login.login';
  static const login = 'login';

}
