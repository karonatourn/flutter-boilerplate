import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/core/services/index.dart';
import 'package:flutter_boilerplate/core/styles/fonts.dart';
import 'package:flutter_boilerplate/core/styles/index.dart';
import 'package:flutter_boilerplate/core/utils/constants.dart';
import 'package:flutter_boilerplate/core/utils/helper_functions.dart';
import 'package:flutter_boilerplate/features/splash/presentation/views/splash_view.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_boilerplate/injection_container.dart' as di;

Future<void> main() async {
  // Initialize dependency injection
  await di.init();

  await LocalizationService().init();

  runApp(LocalizationService().attach(
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final locale = context.locale;
    final fontFamily = AppFonts.getFontFamilyByLanguageCode(locale.languageCode);

    return MaterialApp(
      title: Constants.appName,
      localizationsDelegates: LocalizationService().getLocalizationDelegates(context),
      supportedLocales: LocalizationService().getSupportLocales(context),
      locale: LocalizationService().getLocale(context),
      debugShowCheckedModeBanner: false,
      theme: HelperFunctions.applyFontFamilyWithTheme(AppThemes.light, fontFamily),
      darkTheme: HelperFunctions.applyFontFamilyWithTheme(AppThemes.dark, fontFamily),
      navigatorKey: NavigationService().navigatorKey,
      home: const SplashView(),
    );
  }
}
