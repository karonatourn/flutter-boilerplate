import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

/// Help debug log message
class Log {

  Log._();

  /// Log info message
  static info(dynamic message) {

    if (kDebugMode) {
      Logger().d(message);
    }

  }

  /// Log error message
  static error(dynamic message) {

    if (kDebugMode) {
      Logger().e(message);
    }

  }

  /// Log warning message
  static warn(dynamic message) {

    if (kDebugMode) {
      Logger().w(message);
    }

  }

}
