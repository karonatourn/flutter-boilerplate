import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';
import 'package:flutter_boilerplate/core/services/index.dart';
import 'package:flutter_boilerplate/features/login/presentation/views/login_view.dart';
import 'package:flutter_boilerplate/generated/locale_keys.g.dart';

class HelperFunctions {

  static ThemeData applyFontFamilyWithTheme (ThemeData baseTheme, String fontFamily) {

    return ThemeData.localize(baseTheme, baseTheme.textTheme.apply(fontFamily: fontFamily));

  }

  static BlocStateFailure handleErrorToState(dynamic error, {bool? navigateToLogin}) {

    if (error is DioError) {
      if (error.response?.statusCode == 401) {
        if (navigateToLogin == true) {
          NavigationService().popToTop();
          NavigationService().replace(const LoginView());
        }

        return UnauthorizedStateFailure(message: error.message, statusCode: error.response?.statusCode);
      } else if (error.type == DioErrorType.response) {
        return BlocStateFailure(message: error.response?.data["message"] ?? LocaleKeys.common_somethingWrong.tr(), statusCode: error.response?.statusCode);
      }

      return BlocStateFailure(message: LocaleKeys.common_somethingWrong.tr(), statusCode: error.response?.statusCode);
    } else {
      return const SomethingWrongStateFailure();
    }

  }

}
