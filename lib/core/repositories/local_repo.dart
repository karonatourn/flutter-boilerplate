import 'package:flutter_boilerplate/core/config/app_config.dart';
import 'package:flutter_boilerplate/core/models/auth_model.dart';
import 'package:flutter_boilerplate/core/models/authorize_model.dart';
import 'package:flutter_boilerplate/core/models/cached_auth_request_model.dart';
import 'package:flutter_boilerplate/core/services/local_storage_service.dart';

class LocalRepo {

  static final LocalRepo _instance = LocalRepo._();

  LocalRepo._();

  factory LocalRepo() {

    return _instance;

  }

  Future<String> getSavedLanguage() async {

    var language = await LocalStorageService().getValue(LocalStorageKey.language.toString(), AppConfig.fallbackLocale.languageCode);

    return language;

  }

  Future<void> saveAuthorize(AuthorizeModel authorize) async {

    await LocalStorageService().setValue(LocalStorageKey.authorize.toString(), authorize.toJson());

  }

  Future<AuthorizeModel?> getSavedAuthorize() async {

    var data = await LocalStorageService().getValue(LocalStorageKey.authorize.toString(), null);

    return data != null ? AuthorizeModel.fromJson(data) : null;

  }

  Future<void> saveAuth(AuthModel auth) async {

    await LocalStorageService().setValue(LocalStorageKey.auth.toString(), auth.toJson());

  }

  Future<AuthModel?> getSavedAuth() async {

    var data = await LocalStorageService().getValue(LocalStorageKey.auth.toString(), null);

    return data != null ? AuthModel.fromJson(data) : null;

  }

  Future<CachedAuthRequestModel?> getCachedAuthRequest() async {

    var data = await LocalStorageService().getValue(LocalStorageKey.authRequestCache.toString());

    return data != null ? CachedAuthRequestModel.fromJson(data) : null;

  }

  Future<void> saveCachedAuthRequest(CachedAuthRequestModel authRequest) async {

    await LocalStorageService().setValue(LocalStorageKey.authRequestCache.toString(), authRequest.toJson());

  }

}