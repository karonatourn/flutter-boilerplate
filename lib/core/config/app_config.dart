import 'dart:ui';

/// Manage the whole application configuration
/// -------------------------------------------------------------------------
class AppConfig {

  /// Http service configuration
  /// -----------------------------------------------------------------------
  static const String? httpApiBaseUrl = "https://nokor-api-dev.codingate.com";
  static const String httpApiKey = "";
  static const String? socketBaseUrl = "";
  static const int socketConnectTimeout = 10000;
  static const int httpConnectTimeout = 10000;
  static const int httpReceiveTimeout = 5000;
  static const int httpSendTimeout = 5000;

  static const String countryCode = "kh";

  /// Google map service configuration
  /// -----------------------------------------------------------------------
  static const String googleMapApiKey = "";

  /// OneSignal service configuration
  /// -----------------------------------------------------------------------
  static const String onesignalAppId = "";

  /// Localization service configuration
  /// -----------------------------------------------------------------------
  static const String localeResourcePath = "lib/assets/translations";
  static const List<Locale> supportLocales = [
    Locale('en', 'US'),
    Locale('km', 'KH'),
  ];
  static const Locale fallbackLocale = Locale('en', 'US');

}
