import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class BlocEvent extends Equatable {
  const BlocEvent();

  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

class ErrorDialogDismissEvent extends BlocEvent {
}