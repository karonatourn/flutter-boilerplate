import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class BlocState extends Equatable {
  const BlocState();

  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

@immutable
class BlocStateInitial extends BlocState {}

@immutable
class BlocStateInProgress extends BlocState {}

@immutable
class BlocStateSuccess extends BlocState {}

@immutable
class BlocStateFailure extends BlocState {

  final String? message;
  final int? statusCode;

  const BlocStateFailure({this.message, this.statusCode,});

  BlocStateFailure.from(BlocStateFailure other): message = other.message, statusCode = other.statusCode;

}

@immutable
class UnauthorizedStateFailure extends BlocStateFailure {

  const UnauthorizedStateFailure({required String message, int? statusCode}) : super(message: message, statusCode: statusCode);

}

@immutable
class SomethingWrongStateFailure extends BlocStateFailure {

  const SomethingWrongStateFailure() : super(message: "Something went wrong.");

}
