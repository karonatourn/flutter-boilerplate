import 'package:flutter_bloc/flutter_bloc.dart';

abstract class CubitBase<State> extends Cubit<State> {

  bool isPending = false;

  CubitBase(State initialState) : super(initialState);

}
