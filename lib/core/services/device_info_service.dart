import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';

class DeviceInfoService {

  static final DeviceInfoService _instance = DeviceInfoService._();

  late DeviceInfoPlugin deviceInfoPlugin;
  late String id;
  late String os;
  late String name;
  late String info;

  DeviceInfoService._() {

    deviceInfoPlugin = DeviceInfoPlugin();

  }

  factory DeviceInfoService() {

    return _instance;

  }

  init() async {

    String defaultInfo = "Unknown";

    if (Platform.isAndroid) {
      final device = await deviceInfoPlugin.androidInfo;
      id = device.id ?? defaultInfo;
      name = device.model ?? defaultInfo;
      os = "Android";
      info = device.manufacturer ?? defaultInfo;
    } else if (Platform.isIOS) {
      final device = await deviceInfoPlugin.iosInfo;
      id = device.identifierForVendor ?? defaultInfo;
      name = device.name ?? defaultInfo;
      os = device.systemName ?? defaultInfo;
      info = device.systemVersion ?? defaultInfo;
    }

  }

}