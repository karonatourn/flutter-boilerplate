import 'package:flutter/material.dart';

/// Navigation service help manage navigating from route to route
class NavigationService {
  static final NavigationService _instance = NavigationService._();
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

  NavigationService._();

  factory NavigationService() {
    return _instance;
  }

  /// Get navigator key and useful to be called in main.dart for navigatorKey of
  /// MaterialApp widget's constructor
  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  /// Get global context
  BuildContext get context => _navigatorKey.currentState!.context;

  Future<dynamic> push(Widget widget) async {

    return _instance._navigatorKey.currentState!.push(MaterialPageRoute(
      builder: (context) {
        return widget;
      },
    ));

  }

  /// Replace current route with another route widget
  Future<dynamic> replace(Widget widget) async {

    return _instance._navigatorKey.currentState!.pushReplacement(MaterialPageRoute(
      builder: (context) {
        return widget;
      },
    ));

  }

  /// Pop current route out and navigate to previous route
  pop() {

    _instance._navigatorKey.currentState!.pop();

  }

  /// Pop to until reaching the first route
  popToTop() {

    _instance._navigatorKey.currentState!.popUntil((route) => route.isFirst);

  }
}
