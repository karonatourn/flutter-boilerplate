import 'dart:convert';

import 'package:flutter_boilerplate/core/utils/log.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';

enum LocalStorageKey {
  encryptionKey,
  auth,
  authorize,
  userProfile,
  authRequestCache,
  language,
}

/// This service contains function helping save data in persistent local storage
class LocalStorageService {
  static final LocalStorageService _instance = LocalStorageService._();

  FlutterSecureStorage? _storage;
  late Box _globalEncryptedBox;
  bool _initialized = false;

  LocalStorageService._() {
    _storage ??= const FlutterSecureStorage();
  }

  factory LocalStorageService() {
    return _instance;
  }

  /// Get storage
  FlutterSecureStorage? get storage => _storage;

  Future<void> init() async {

    if (_initialized) {
      return;
    }

    const FlutterSecureStorage secureStorage = FlutterSecureStorage();

    var savedKey = await secureStorage.read(key: LocalStorageKey.encryptionKey.toString());
    if (savedKey == null) {
      final key = Hive.generateSecureKey();
      await secureStorage.write(key: LocalStorageKey.encryptionKey.toString(), value: base64UrlEncode(key));
    }

    final encryptionKey = base64Url.decode(savedKey!);

    await Hive.initFlutter();

    _globalEncryptedBox = await Hive.openBox('globalStorage', encryptionCipher: HiveAesCipher(encryptionKey));

    _initialized = true;

    Log.info("Local storage service is initialized successfully");

  }

  /// Set value to local storage by key
  Future<void> setValue(dynamic key, dynamic value) async {

    await init();

    await _globalEncryptedBox.put(key, value);

  }

  /// Get value from local storage by key
  Future<dynamic> getValue(dynamic key, [dynamic defaultValue]) async {

    await init();

    return _globalEncryptedBox.get(key, defaultValue: defaultValue);

  }

  /// Delete value from local storage by key
  Future<void> deleteValue(dynamic key) async {

    await init();

    await _globalEncryptedBox.delete(key);

  }

  /// Clear all saved value from the local storage
  Future<void> clear() async {

    await init();

    await _globalEncryptedBox.clear();

  }

}
