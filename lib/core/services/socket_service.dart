import 'package:flutter/foundation.dart';
import 'package:flutter_boilerplate/core/config/app_config.dart';
import 'package:logger/logger.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketService {
  static final SocketService _instance = SocketService._();

  final Map<String, List<void Function(dynamic data)>> _listeners = {};
  Socket? _socket;

  SocketService._();

  factory SocketService () {

    return _instance;

  }

  Future<void> init() async {

    if (AppConfig.socketBaseUrl == null || AppConfig.socketBaseUrl!.isEmpty) {
      throw Exception("Socket base url is required");
    }

    if (_socket != null) {
      _socket!.disconnect();
      _socket!.dispose();
      _socket = null;
    }

    _socket = io(AppConfig.socketBaseUrl, OptionBuilder()
        .enableReconnection()
        .enableAutoConnect()
        .setTimeout(AppConfig.socketConnectTimeout)
        .build());

    _socket!..on("connect", (data) {
      Logger().d("Socket connected");
    })
    ..on("reconnect", (data) {
      Logger().d("Socket reconnect");
    });

    // Listen other events
    // _socket?.on("event_name", (data) {
    //   // Do something here
    // });

  }
  
  void join(dynamic event) {
    
    _socket?.emit("join", {
      "event": event
    });
    
  }

  void leave(dynamic event) {

    _socket?.emit("leave", {
      "event": event
    });

  }

  VoidCallback addListener(String eventName, void Function(dynamic data) listener) {

    if (!_listeners.containsKey(eventName)) {
      _socket?.on(eventName, (data) {

        var listeners = _listeners[eventName] ?? [];

        for(var listener in listeners) {
          listener(data);
        }

      });

      _listeners[eventName] = [];
    }

    _listeners[eventName]!.add(listener);

    void remove() {
      _listeners[eventName]!.remove(listener);
    }

    return remove;
  }

  void removeListener(String eventName, void Function(dynamic data) listener) {

    if (_listeners.containsKey(eventName)) {
      _listeners[eventName]!.remove(listener);
    }

  }

}
