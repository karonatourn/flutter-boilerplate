import 'package:flutter/widgets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_boilerplate/core/config/app_config.dart';

/// This service will help for localization configuration and will be used in main.dart
class LocalizationService {
  static final LocalizationService _instance = LocalizationService._();
  bool _isInitialized = false;

  LocalizationService._();

  factory LocalizationService() {
    return _instance;
  }

  /// Invoked in main function of main.dart
  init() async {

    if (!_isInitialized) {
      WidgetsFlutterBinding.ensureInitialized();
      await EasyLocalization.ensureInitialized();

      _isInitialized = true;
    }

  }

  /// Invoked with runApp function in main.dart
  attach({required Widget child}) {

    return EasyLocalization(
      supportedLocales: AppConfig.supportLocales,
      path: AppConfig.localeResourcePath,
      fallbackLocale: AppConfig.fallbackLocale,
      child: child,
    );

  }

  /// Invoked and return delegates used by localizationsDelegates in MaterialApp constructor
  ///
  /// ```dart
  /// MaterialApp(
  //    localizationsDelegates: LocalizationService.getLocalizationDelegates(context),
  //  )
  // ```
  getLocalizationDelegates(BuildContext context) {

    return context.localizationDelegates;

  }

  /// Invoked and return delegates used by localizationsDelegates in MaterialApp constructor
  ///
  /// ```dart
  /// MaterialApp(
  //    supportedLocales: LocalizationService.getSupportLocales(context),
  //  )
  // ```
  getSupportLocales(BuildContext context) {

    return context.supportedLocales;

  }

  /// Get current locale
  getLocale(BuildContext context) {

    return context.locale;

  }
}
