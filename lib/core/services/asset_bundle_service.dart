import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

/// This service contains functions working with asset bundle
class AssetBundleService {
  static final AssetBundleService _instance = AssetBundleService._();

  AssetBundleService._();

  factory AssetBundleService () {
    return _instance;
  }

  /// Load json bundle asset and then parse it to Map object
  Future<Map<String, dynamic>> loadJsonAsset(String path) async {

    final content = await rootBundle.loadString(path);
    final object = json.decode(content);

    return object;

  }
}
