import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_boilerplate/core/config/app_config.dart';
import 'package:flutter_boilerplate/core/services/http/authorization_interceptor.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

/// Abstract class for every Http client service that can extends from
abstract class HttpServiceBase<T> {

  late final T _httpClient;

  T get httpClient => _httpClient;

  Future<void> init();

  void configureHttpClient(T httpClient);

  T createNewHttpClient();

}

class CustomHttpHeaderKey {
  static const String authorize = "Authorization";
  static const String auth = "Auth";
  static const String locale = "Locale";
}

/// Http service with Dio
class HttpService extends HttpServiceBase<Dio> {

  static final HttpService _instance = HttpService._();

  HttpService._() {

    _httpClient = createNewHttpClient();

  }

  factory HttpService() {

    return _instance;

  }

  @override
  Future<void> init() async {
  }

  /// Called to configure the provided dio instance with the same configuration options and more.
  @override
  void configureHttpClient(Dio httpClient) {

    if (AppConfig.httpApiBaseUrl == null || AppConfig.httpApiBaseUrl!.isEmpty) {
      throw Exception("Http base url is required");
    }

    httpClient.options..baseUrl = "${AppConfig.httpApiBaseUrl}/api"
      ..connectTimeout = AppConfig.httpConnectTimeout
      ..receiveTimeout = AppConfig.httpReceiveTimeout
      ..sendTimeout = AppConfig.httpSendTimeout
      ..headers["Content-Type"] = "application/json"
      ..headers["Accept"] = "application/json";

    httpClient.interceptors.add(AuthorizationInterceptor(httpClient));

    // Add interceptor for pretty logger for request and response
    if (!kReleaseMode) {
      httpClient.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true,
      ));
    }

  }

  /// Help create a new instance of Http client instance
  @override
  Dio createNewHttpClient() {

    Dio dio = Dio();
    configureHttpClient(dio);

    return dio;

  }

  Future<Response<T>> authGet<T>(
    String path, {
      Map<String, dynamic>? queryParameters,
      Options? options,
      CancelToken? cancelToken,
      ProgressCallback? onReceiveProgress,
    }) async {

    options ??= Options();
    options.extra ??= {};
    options.extra![AuthOptionExtraKey.requiredAuth.toString()] = true;

    return _httpClient.get<T>(path,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onReceiveProgress: onReceiveProgress,
    );

  }

  Future<Response<T>> authPost<T>(
    String path, {
      data,
      Map<String, dynamic>? queryParameters,
      Options? options,
      CancelToken? cancelToken,
      ProgressCallback? onSendProgress,
      ProgressCallback? onReceiveProgress,
    }) async {

    options ??= Options();
    options.extra ??= {};
    options.extra![AuthOptionExtraKey.requiredAuth.toString()] = true;

    return _httpClient.post<T>(path,
      data: data,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );

  }

}
