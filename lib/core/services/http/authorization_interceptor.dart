import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_boilerplate/core/config/app_config.dart';
import 'package:flutter_boilerplate/core/models/auth_model.dart';
import 'package:flutter_boilerplate/core/models/authorize_model.dart';
import 'package:flutter_boilerplate/core/models/authorize_request_model.dart';
import 'package:flutter_boilerplate/core/models/auth_request_model.dart';
import 'package:flutter_boilerplate/core/repositories/local_repo.dart';
import 'package:flutter_boilerplate/core/services/device_info_service.dart';
import 'package:flutter_boilerplate/core/services/http/http_service.dart';
import 'package:flutter_boilerplate/core/services/local_storage_service.dart';
import 'package:logger/logger.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/http.dart';

enum AuthOptionExtraKey {
  requiredAuth,
}

/// The interceptor for ensuring Auth and Authorize is set into the request header when required
class AuthorizationInterceptor extends QueuedInterceptorsWrapper {
  
  late Dio _dio;
  late Dio _tokenDio;

  AuthorizeModel? _authorization;
  AuthModel? _auth;

  AuthorizationInterceptor(Dio dio) {

    _dio = dio;

    _tokenDio = Dio();
    _tokenDio.options = _dio.options;

    if (!kReleaseMode) {
      _integrateLogger(_tokenDio);
    }
    
  }
  
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {

    // Set language code to header of the request
    var language = await LocalRepo().getSavedLanguage();
    options.headers[CustomHttpHeaderKey.locale] = language;

    if (_authorization == null) {
      _lock();
      _authorization = await LocalRepo().getSavedAuthorize();

      if (_authorization == null) {
        Logger().d("You don't have authorize yet. Now we are processing request new authorize.");

        var hasError = await _requestAuthorize().then((response) async {

          var data = response.data["data"];

          await LocalStorageService().setValue(LocalStorageKey.authorize.toString(), data);
          _authorization = AuthorizeModel.fromJson(data);
          options.headers[CustomHttpHeaderKey.authorize] = _authorization?.token;
          Logger().d("Authorize has been created: ${_authorization?.token}");

          return Future.value(false);

        }).catchError((error, stackTrace) {

          handler.reject(error, false);
          return Future.value(true);

        }).whenComplete(() {

          _unlock();

        });

        if (hasError) {
          return;
        }
      }

      _unlock();
    }

    if (_isRequiredAuth(options)) {

      /* Below will try to get cached login info credential from the locale storage
          in order to request login again to get new access token. This way is not recommended, but we just force
          to do this as we currently don't have API to refresh new access token */
      if (_auth == null) {
        _lock();
        _auth = await LocalRepo().getSavedAuth();

        if (_auth == null) {
          var cachedLoginInfo = await LocalRepo().getCachedAuthRequest();

          if (cachedLoginInfo == null) {
            Logger().w("No saved login info for renewing auth");
            _unlock();
            handler.next(options);
            return;
          }

          var hasError = await _requestAuth(AuthRequestModel(
            phoneNumber: cachedLoginInfo.phoneNumber,
            password: cachedLoginInfo.password,
          )).then((response) async {

            _auth = AuthModel.fromJson(response.data["data"]);
            await LocalRepo().saveAuth(_auth!);
            Logger().d("Auth has been created: ${_auth?.accessToken}");

            return Future.value(false);

          }).catchError((error, stackTrace) {

            handler.reject(error, false);
            return Future.value(true);

          }).whenComplete(() {
            _unlock();
          });

          if (hasError) {
            return;
          }
        }

        _unlock();
      }

      options.headers[CustomHttpHeaderKey.auth] = _auth?.accessToken;
      Logger().d("Auth added to header: ${_auth?.accessToken}");
    }

    options.headers[CustomHttpHeaderKey.authorize] = _authorization?.token;
    Logger().d("Authorize added to header: ${_authorization?.token}");

    handler.next(options);
    
  }
  
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    if (err.response?.statusCode == 401) {
      _lock();

      var options = err.requestOptions;

      var hasError = await _requestAuthorize().then((response) async {

        _authorization = AuthorizeModel.fromJson(response.data["data"]);
        await LocalRepo().saveAuthorize(_authorization!);
        options.headers[CustomHttpHeaderKey.authorize] = _authorization?.token;
        Logger().d("Authorize has been created: ${_authorization?.token}");

        return Future.value(false);

      }).catchError((error, stackTrace) async {

        handler.reject(error);
        return Future.value(true);

      }).whenComplete(() {
        _unlock();
      });
      
      if (hasError) {
        return;
      }

      if (_isRequiredAuth(options)) {
        _lock();

        var cachedLoginInfo = await LocalRepo().getCachedAuthRequest();

        if (cachedLoginInfo != null) {

          hasError = await _requestAuth(AuthRequestModel(
            phoneNumber: cachedLoginInfo.phoneNumber,
            password: cachedLoginInfo.password,
          )).then((response) async {

            _auth = AuthModel.fromJson(response.data["data"]);
            await LocalRepo().saveAuth(_auth!);
            options.headers[CustomHttpHeaderKey.auth] = _auth?.accessToken;
            Logger().d("Auth has been created: ${_auth?.accessToken}");

            return Future.value(false);

          }).catchError((error, stackTrace) {

            handler.reject(error);
            return Future.value(true);

          }).whenComplete(() {
            _unlock();
          });

          if (hasError) {
            return;
          }

        }

      }

      // Resend the request again
      await _sendRequestByOptions(options).then((response) {

        handler.resolve(response);

      }).catchError((error) {

        handler.next(error);

      });

      return;
    }

    handler.next(err);
  }

  void _lock () {

    _dio.lock();

  }

  void _unlock () {

    _dio.unlock();

  }

  bool _isRequiredAuth(RequestOptions options) {

    var isRequired = options.extra.containsKey(AuthOptionExtraKey.requiredAuth.toString())
        && options.extra[AuthOptionExtraKey.requiredAuth.toString()] == true;

    return isRequired;

  }

  Future<Response<dynamic>> _sendRequestByOptions(RequestOptions options) {

    var dio = Dio();
    dio.options.baseUrl = AppConfig.httpApiBaseUrl!;

    if (!kReleaseMode) {
      _integrateLogger(dio);
    }

    return dio.request(options.path,
      options: Options(
        headers: options.headers,
        method: options.method,
        receiveTimeout: options.receiveTimeout,
        sendTimeout: options.sendTimeout,
        extra: options.extra,
      ),
      data: options.method == HttpMethod.GET ? null : options.data,
      queryParameters: options.queryParameters,
    );

  }

  Future<Response<dynamic>> _requestAuth(AuthRequestModel data) async {

    data.countryCode = AppConfig.countryCode;

    return _tokenDio.post("/authorize",
      data: data.toJson(),
    );

  }

  Future<Response<dynamic>> _requestAuthorize() async {

    var deviceInfo = DeviceInfoService();

    return _tokenDio.post("/authorize",
      data: AuthorizeRequestModel(
        deviceId: deviceInfo.id,
        deviceName: deviceInfo.name,
        deviceOs: deviceInfo.os,
        deviceInfo: deviceInfo.info,
      ).toJson(),
    );

  }

  _integrateLogger(Dio dio) {

    dio.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: true,
      error: true,
      compact: true,
    ));

  }
  
}