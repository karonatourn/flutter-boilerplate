import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';

class NetworkInfoService {
  static final NetworkInfoService _instance = NetworkInfoService._();
  late Connectivity _connectivity;

  NetworkInfoService._() {

    _connectivity = Connectivity();

  }

  /// Factory for the instance
  factory NetworkInfoService() {

    return _instance;

  }

  /// Return true/false telling if the connectivity is connected
  Future<bool> get isConnected async {

    var result = await _connectivity.checkConnectivity();

    return result != ConnectivityResult.none;

  }

  /// Add a listener trigger when connectivity changed
  StreamSubscription addChangeListener (void Function(ConnectivityResult result)? onChanged) {

    return _connectivity.onConnectivityChanged.listen(onChanged);

  }
}
