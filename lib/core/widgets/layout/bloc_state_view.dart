import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_event.dart';
import 'package:flutter_boilerplate/core/bloc/bloc_state.dart';
import 'package:flutter_boilerplate/generated/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

typedef InitStateCallback = void Function(BuildContext context);

class BlocStateView<BlocType extends Bloc<BlocEvent, BlocState>> extends StatefulWidget {
  final InitStateCallback? onInitState;
  final BlocWidgetBuilder<BlocState>? builder;

  const BlocStateView({
    Key? key,
    this.onInitState,
    this.builder,
  }) : super(key: key);

  @override
  _BlocStateViewState<BlocType> createState() => _BlocStateViewState<BlocType>();
}

class _BlocStateViewState<BlocType extends Bloc<BlocEvent, BlocState>> extends State<BlocStateView> {
  @override
  void initState() {
    super.initState();
    widget.onInitState?.call(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocType, BlocState>(
      builder: (BuildContext context, BlocState state) {
        return Material(
          child: Stack(
            children: [
              if (widget.builder != null)
                widget.builder!.call(context, state),
              if (state is BlocStateInProgress)
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  alignment: Alignment.center,
                  color: Colors.black45,
                  child: Container(
                    width: 200,
                    height: 180,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      color: Colors.white,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(LocaleKeys.common_pleaseWait.tr()),
                        const SizedBox(height: 20),
                        const CircularProgressIndicator(),
                        const SizedBox(height: 20),
                        Text(
                          LocaleKeys.common_processingYourRequest.tr(),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              if (state is BlocStateFailure)
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  alignment: Alignment.center,
                  color: Colors.black45,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width - 80,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: Colors.white,
                        ),
                        alignment: Alignment.center,
                        padding: const EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(state.message ?? LocaleKeys.common_somethingWrong.tr(), style: Theme.of(context).textTheme.bodyText2),
                            TextButton(
                              onPressed: () {
                                context.read<BlocType>().add(ErrorDialogDismissEvent());
                              },
                              child: Text(LocaleKeys.common_buttonClose.tr().toUpperCase(), style: Theme.of(context).textTheme.button,),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
