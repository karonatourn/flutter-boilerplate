import 'package:equatable/equatable.dart';

abstract class UseCase<Type, P extends Params> {

  Future<Type> call([P? params]);

}

abstract class Params extends Equatable {

  const Params();

  @override
  List<Object?> get props => [];

}

class NoParams extends Params {

  const NoParams();

}

class PaginationParams extends Params {

  final int offset;
  final int limit;
  final String? search;

  const PaginationParams({
    required this.offset,
    required this.limit,
    this.search,
  });

}
