class AuthorizeRequestModel {
  AuthorizeRequestModel({
      this.deviceId, 
      this.deviceName, 
      this.deviceInfo, 
      this.deviceOs,});

  AuthorizeRequestModel.fromJson(dynamic json) {
    deviceId = json['device_id'];
    deviceName = json['device_name'];
    deviceInfo = json['device_info'];
    deviceOs = json['device_os'];
  }
  String? deviceId;
  String? deviceName;
  String? deviceInfo;
  String? deviceOs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['device_id'] = deviceId;
    map['device_name'] = deviceName;
    map['device_info'] = deviceInfo;
    map['device_os'] = deviceOs;
    return map;
  }

}