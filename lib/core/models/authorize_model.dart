class AuthorizeModel {
  AuthorizeModel({
      this.token, 
      this.expiredAt,});

  AuthorizeModel.fromJson(dynamic json) {
    token = json['token'];
    expiredAt = json['expired_at'];
  }
  String? token;
  int? expiredAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    map['expired_at'] = expiredAt;
    return map;
  }

}