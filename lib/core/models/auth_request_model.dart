class AuthRequestModel {
  AuthRequestModel({
      this.phoneNumber, 
      this.password, 
      this.countryCode,});

  AuthRequestModel.fromJson(dynamic json) {
    phoneNumber = json['phone_number'];
    password = json['password'];
    countryCode = json['country_code'];
  }
  String? phoneNumber;
  String? password;
  String? countryCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['phone_number'] = phoneNumber;
    map['password'] = password;
    map['country_code'] = countryCode;
    return map;
  }

}