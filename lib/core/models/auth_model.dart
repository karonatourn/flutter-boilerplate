class AuthModel {
  AuthModel({
      this.accessToken, 
      this.expiredAt,});

  AuthModel.fromJson(dynamic json) {
    accessToken = json['access_token'];
    expiredAt = json['expired_at'];
  }
  String? accessToken;
  int? expiredAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['access_token'] = accessToken;
    map['expired_at'] = expiredAt;
    return map;
  }

}