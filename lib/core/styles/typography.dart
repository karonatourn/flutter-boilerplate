class AppFontSizes {
  static const double headline1 = 96.0;
  static const double headline2 = 60.0;
  static const double headline3 = 48.0;
  static const double headline4 = 34.0;
  static const double headline5 = 24.0;
  static const double headline6 = 20.0;

  static const double subtitle1 = 16.0;
  static const double subtitle2 = 14.0;

  static const double bodyText1 = 16.0;
  static const double bodyText2 = 14.0;

  static const double button = 14.0;

  static const double caption = 12.0;

  static const double overLine = 10.0;
}
