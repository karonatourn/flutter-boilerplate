import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xFF6B4F4F);
  static const Color screenBackground = Color(0xFFEED6C4);
}
