class AppFonts {

  static final Map<String, String> _localizedFontFamilies = {
    "en": "Roboto",
    "km": "KohSantepheap",
  };

  static String getFontFamilyByLanguageCode(String code) {

    return _localizedFontFamilies[code] ?? "Roboto";

  }

}