import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/core/styles/colors.dart';

class AppThemes {

  static final ThemeData light = ThemeData(
    brightness: Brightness.light,
    primaryColor: AppColors.primary,
  );

  static final ThemeData dark = ThemeData(
    brightness: Brightness.dark,
    primaryColor: AppColors.primary,
  );

}
