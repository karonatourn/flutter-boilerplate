[Back](../README.md)

# Easy Localization

### Introduction

Manage localization in the app easier. All localization for every feature in the app are stored in JSON files at path `lib/assets/translations/`.
Naming convention for translation key in the JSON files use *camelCase*.

### Installation

```
flutter pub add easy_localization
```

### Implementation

After updating the translation JSON files, you have to run the command below

```bash
flutter pub run easy_localization:generate --source-dir lib/assets/translations
flutter pub run easy_localization:generate -f keys -o locale_keys.g.dart -S lib/assets/translations
```

To use the localization in the widget, you do the following things

```dart
import 'package:flutter_boilerplate/generated/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

// In widget
Text(LocaleKeys.username.tr())
```

Look at [here](https://pub.dev/packages/easy_localization)

