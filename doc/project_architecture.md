[Back](../README.md)

# Project Architecture

### Introduction

This project follow the Clean Architecture that causes some of the following advantages.
Read [here](https://medium.com/@fakhiradevina/flutter-tdd-clean-architecture-272373727699)

- Separated concerns
- Testable
- Scalable

<img src="./images/project_architecture.png" alt="image" width="300"/>

Clean Architecture has 3 layers:

### **1. Presentation Layer**

The role of the presentation layer is to display the application data on the screen. Whenever the data changes, either due to user interaction (such as pressing a button) or external input (such as a network response), the UI should update to reflect the changes.

### **2. Domain Layer**

The domain layer is responsible for encapsulating complex business logic, or simple business logic that is reused by multiple Bloc.

A domain layer provides the following benefits:

- It avoids code duplication.
- It improves readability in classes that use domain layer classes.
- It improves the testability of the app.
- It avoids large classes by allowing you to split responsibilities.

The layer containers sub-folders like the following example:

**/domain/entities**
```dart
class Product {

  final int id;
  final String name;
  final String price;

}
```

**/domain/respositories**
```dart
abstract class ProductRepository {

  Future<Product> getProducts();

}
```

**/domain/usecases**
```dart
class GetProductUseCase extends UseCase<Product> {

  final ProductRepository repo;

  GetProductUseCase(this.repo);

  @override
  Future<Product> call() async {

    return repo.getProducts();

  }

}
```

### **3. Data Layer**

While the presentation layer contains UI-related state and UI logic, the data layer contains application data and business logic. The business logic is what gives value to your app—it's made of real-world business rules that determine how application data must be created, stored, and changed.

The data layer is made of repositories that each can contain zero to many data sources. You should create a repository class for each different type of data you handle in your app.

**/domain/datasources**
```dart
abstract class ProductRemoteDataSource {
  Future<Product> postLogin({required String phoneNumber, required String password});
}

class ProductRemoteDataSourceImpl extends ProductRemoteDataSource {

  @override
  Future<Product> getProducts() async {

    final response = await HttpService().httpClient.get('/client/auth/login');

    final data = Product.fromJson(response.data["data"]);

    return data;

  }

}
```

**/domain/respositories**
```dart
class ProductRepositoryImpl extends ProductRepository {

  final ProductRemoteDataSource productRemoteDataSource;

  Future<Product> getProducts() {

    final products = await productRemoteDataSource.getProducts();

    return products;

  }

}
```