# Project Structure

[Back](../README.md)

Have well project structure organized can make new team onboard easlier and script files are placed in proper places that are easy for team to find.

<img src="./images/project_structure.png" alt="image" width="300"/>

**/lib** : it is main folder storing all main source codes of the app

**/assets** : it contains assets like image files, translation files, fonts, etc

**/core** : it stores all sub-folders containing utilty functions, configuration, services, styles, widgets, etc that can be reusable around the whole app with 1 or more features

**/bloc** : 

**/config** : 

**/errors** : 

**/models** : 

**/repositories** : 

**/services** : 

**/styles** : 

**/usecases** : 

**/utils** : 

**/widgets** : 

**/features** : 

A feature folder contains the following sub-folders

**/data** : 

**/repositories** : 

**/usecases** : 

**/presentation** : 
