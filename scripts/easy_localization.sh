#!/bin/sh

flutter pub run easy_localization:generate --source-dir lib/assets/translations

flutter pub run easy_localization:generate -f keys -o locale_keys.g.dart -S lib/assets/translations