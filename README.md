# Project Name

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Set up Flutter environment](https://docs.flutter.dev/get-started/install)
- [Set up an editor](https://docs.flutter.dev/get-started/editor)
- [Dio HTTP](https://pub.dev/packages/dio)
- [Easy Localization](./doc/easy_localization.md)
- [Bloc State Management](https://bloclibrary.dev/#/gettingstarted)
- [Persistent Local Storage with Hive](https://docs.hivedb.dev/#/) 
- [Service Locator with GetIt](https://pub.dev/packages/get_it)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

More samples for development

- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

## How to run the project

Connect your devices using USB with Debugging enabled or open an Emulator or Simulator

```dart
flutter run
```

## Dependencies

Flutter supports using shared packages contributed by other developers to the Flutter and Dart ecosystems. This allows quickly building an app without having to develop everything from scratch.

```dart
flutter pub add dependency_name
```

More details about dependencies, read [here](https://docs.flutter.dev/development/packages-and-plugins/using-packages)

## Project Structure

Have well project structure organized can make new team onboard easlier and script files are placed in proper places that are easy for team to find. [Read more](./doc/project_structure.md)

## Project Architecture

Clean architecture is to provide developers with a way to organize code in such a way that it encapsulates the business logic but keeps it separate from the delivery mechanism. [Read more](./doc/project_architecture.md)
